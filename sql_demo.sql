CREATE TABLE shopping (id INTEGER PRIMARY KEY, name TEXT, amount
INTEGER);

DROP TABLE shopping;

-- ALTER table shopp RENAME to shopping

INSERT INTO shopping VALUES (1, 'Avokado', 5);
INSERT INTO shopping VALUES (2, 'Milk', 2);
INSERT INTO shopping VALUES (3, 'Bread', 3);
INSERT INTO shopping VALUES (4, 'Chocolate', 8);
INSERT INTO shopping VALUES (5, 'Bamba', 5);
INSERT INTO shopping VALUES (6, 'Orange', 10);

DELETE from shopping WHERE name like 'Orange';

UPDATE shopping SET name = 'Bisli' WHERE name LIKE 'Bamba';

UPDATE shopping SET amount=1 WHERE name LIKE 'Milk';

ALTER TABLE shopping ADD COLUMN maavar;

UPDATE shopping SET MAAVAR = 0;

UPDATE shopping SET maavar=6 WHERE id=1;
UPDATE shopping SET maavar=3 WHERE id=2;
UPDATE shopping SET maavar=12 WHERE id=3;
UPDATE shopping SET maavar=8 WHERE id=4;
UPDATE shopping SET maavar=5 WHERE id=5;

SELECT * FROM shopping WHERE maavar BETWEEN 3 AND 5

select * from shopping
order by amount desc

CREATE TABLE books (id INTEGER PRIMARY KEY, name TEXT);
INSERT INTO books VALUES (1, 'SQL PROGRAMMING');
INSERT INTO books VALUES (2, 'CSHARP PROGRAMMING');
DELETE FROM books;

SELECT COUNT(*) TOTAL from shopping;
SELECT MAX(amount) from shopping;
SELECT AVG(amount) from shopping;
SELECT MIN(amount) from shopping;

update shopping
set maavar = 12
where name = 'Milk';

Select maavar, COUNT(*) TOTAL_PRODUCTS, MAX(amount) HIGHEST_AMOUNT_OF_PRODUCTS_FOR_SPECIFIC_ITEM_IN_THIS_MAAVAR
FROM shopping
GROUP BY maavar
having TOTAL_PRODUCTS >= 2

CREATE TABLE prices (id INTEGER PRIMARY KEY, price INTEGER);

INSERT INTO prices VALUES (1, 3);
INSERT INTO prices VALUES (2, 7);
INSERT INTO prices VALUES (3, 12);
INSERT INTO prices VALUES (4, 5);
INSERT INTO prices VALUES (5, 3);
INSERT INTO prices VALUES (6, 2);
INSERT INTO prices VALUES (7, 10);

delete from prices
where id = 1

select maavar, sum(sum_product) from
(select shopping.id,name, amount, maavar, prices.price, amount * prices.price as sum_product from shopping join prices
on shopping.id = prices.id)
group by maavar

select shopping.id,name, amount, maavar, prices.price, amount * prices.price as sum_product from shopping join prices
on shopping.id = prices.id

select s.id,name, amount, maavar, p.price  from shopping s join prices p
on s.id = p.id;

select * from prices;

SELECT s.id, s.name, s.amount, s.maavar, p.price FROM shopping s JOIN
prices p ON s.id=p.id WHERE p.price = (SELECT MAX(price) FROM
prices)


-- write query to find the most expansive price in each maavar
-- write query to find the avg price in each maavar
-- create table Expansive_products 
   --id, name, price
   -- will contain INSERT the 3 most exapnsive products
   -- hint: limit, order by, desc

-- ============================= SOLUTION 

-- 1 + 2
select maavar, count(*) TOTAL, max(price) MAX_PRICE, avg(price) AVG_PRICE from
(SELECT s.id, s.name, s.amount, s.maavar, p.price FROM shopping s JOIN
prices p ON s.id=p.id WHERE p.price)
group by maavar

-- 3
CREATE TABLE EXPANSIVE_PRODUCTS (id INTEGER PRIMARY KEY, name TEXT, price INTEGER);
INSERT INTO EXPANSIVE_PRODUCTS
SELECT s.id, s.name, p.price FROM shopping s JOIN
prices p ON s.id=p.id 
order by price desc
limit 3



