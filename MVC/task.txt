take last lesson code 
https://gitlab.com/dotnet290320/08.11.2020/-/blob/master/SqliteCSharp.cs

create POCO class 
Employee.cs
-ctor default
-ctor all fields
-ToString()
-property per field:
 for exmaple:
    public int Id { get; set;}
    ...

create class called:
EmployeeDAO:
  public List<Employee> GetEmployees()
  {
      ...
  }
  public Employee GetEmployeeById(int id)
  {
    // select where id = ...
  }
  public void UpdateEmployee(Employee e, int id)
  {
    // ...
  }
  public void DeleteEmployee(int id)
  {
    // ...
  }
  }

  Program :
    call GetEmployees and print it to the screen